/**
 * Tows car from section to section
 */
public class Vehicle implements Runnable {

	private Section behind;
	private Section front;
	private Car towedCar;
	private Thread t;
	private int number;

    public Vehicle(int number, Section behind, Section front)
    {
		this.behind = behind;
	    this.front = front;
	    this.number = number;
    }

    @Override
    public void run() {

	    while (true) {
		    synchronized (behind)
		    {
			    // wait for car to come behind
			    while (!behind.isOccupied()) {
				    try {
					    behind.wait();
				    }
				    catch (InterruptedException e) {
					    e.printStackTrace();
				    }
			    }
			    if (behind.isOccupied()) {
				    // tow the car from behind
				    towedCar = behind.unloadCar();
			    }
			    behind.notifyAll();
		    }
		    // avoid waiting for front section if not towing car
		    while (towedCar!=null) {
			    synchronized (front)
			    {
				    // wait for front section to be free
				    while(front.isOccupied()) {
					    try {
						    front.wait();
					    }
					    catch (InterruptedException e) {
						    e.printStackTrace();
					    }
				    }
				    // if front is free
				    if (!front.isOccupied()) {
					    try {
						    // take time to tow
						    t.sleep(Param.TOWING_TIME);
						    // leave the car and release it
						    front.loadCar(towedCar);
						    towedCar = null;
					    }
					    catch (InterruptedException e) {
						    e.printStackTrace();
					    }
				    }
				    front.notifyAll();
			    }
		    }

	    }
    }

	public void start()
	{
		if (t == null) {
			t = new Thread(this);
			t.start();
		}
		System.out.printf("Vehicle %d starts working...\n", number);
	}
	
	public void interrupt() {
		t.interrupt();
	}

	public boolean isAlive() {
		return t.isAlive();
	}
}
