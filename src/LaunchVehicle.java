/**
 * Created by Jone on 2/05/2016.
 */
public class LaunchVehicle implements Runnable {

	private Lift lift;
	private Section section;
	private Thread t;
	private Car towedCar;

	public LaunchVehicle(Lift lift, Section section)
	{
		this.lift = lift;
		this.section = section;
		towedCar = null;
	}

	@Override
	public void run()
	{
		while (true) {
			synchronized (lift)
			{
				// wait for lift
				while (lift.getFloor()!=Param.FIRST_FLOOR && !lift.hasInbound()) {
					try {
						lift.wait();
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				// check if conditions matches
				if (lift.getFloor()==Param.FIRST_FLOOR && lift.hasInbound()) {
					// unload car and cache the car, so that lift can be release
					towedCar = lift.unloadCar();
					towedCar.setBound(Param.Bound.OUTBOUND);
				}
				lift.notifyAll();
			}
			// avoid waiting for section when not towing car
			while (towedCar!=null) {
				synchronized (section)
				{
					while (section.isOccupied()) {
						try {
							section.wait();
						}
						catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					if (!section.isOccupied()) {
						try {
							// take time to tow
							t.sleep(Param.TOWING_TIME);
							// leave the car and release it
							section.loadCar(towedCar);
							towedCar = null;
						}
						catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					section.notifyAll();
				}
			}
		}
	}

	public void start()
	{

		if (t == null) {
			t = new Thread(this);
			t.start();
		}
		System.out.println("Launcher starts working...");
	}
	
	public void interrupt() {
		t.interrupt();
	}

}
