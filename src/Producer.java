
/**
 * Generate a car to wait for the lift, and load the car into the lift.
 */
public class Producer implements Runnable {

    private Lift lift;
	private Thread t;
	public static boolean hasArrival;

    public Producer(Lift lift)
    {

	    this.lift = lift;
	    hasArrival = false;
    }

    @Override
    public void run() {

	    while (true)
	    {
		    // get new car
		    Car car = Car.getNewCar();
			hasArrival = true;
		    // keep waiting for lift while car is here
		    while (car != null) {
			    synchronized (lift)
			    {
				    while (lift.getFloor()!=Param.GROUND_FLOOR &&
						    lift.hasInbound() && lift.hasOutbound()) {
					    try {
						    lift.wait();
					    } catch (InterruptedException e) {
						    e.printStackTrace();
					    }
				    }
                    // check if lift is at ground floor and unoccupied
				    if (lift.getFloor()==Param.GROUND_FLOOR &&
						    !lift.hasOutbound() && !lift.hasInbound())
				    {
					    // load car to lift
					    lift.loadCar(car);
					    try {
						    // signal lift to go up...
						    lift.goUp(t);
					    }
					    catch (InterruptedException e) {
						    e.printStackTrace();
					    }
                        // release the produced car and cache the signal for operator
					    car = null;
					    hasArrival = false;
				    }
				    lift.notifyAll();
			    }
		    }
		    // wait for next car arrive
		    try {
			    t.sleep(Param.arrivalLapse());
		    }
		    catch (Exception e) {

		    }

	    }
    }

    public void start()
    {

        if (t == null) {
	        t = new Thread(this);
	        t.start();
        }
	    System.out.println("Producer starts working...");
    }
	
	public void interrupt() {
		t.interrupt();
	}

	public boolean isAlive() {
		return t.isAlive();
	}
}
