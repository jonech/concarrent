/**
 * Created by Jone on 30/04/2016.
 */
public class Operator implements Runnable{

    private Lift lift;
	private Thread t;

    public Operator(Lift lift)
    {
        this.lift = lift;
    }

	@Override
	public void run() {

		while (true) {

			//System.out.println("operator sleeping");
			try {
				t.sleep(Param.operateLapse());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			synchronized (lift)
			{
				// wait if lift is occupied and car arrives at ground floor
				while (lift.hasInbound() && lift.hasOutbound() && !Producer.hasArrival) {
					try {
						t.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}

				if (!lift.hasOutbound() && !lift.hasInbound()) {
					// give priority to arrival car
					if (Producer.hasArrival && lift.getFloor()!=Param.GROUND_FLOOR) {
						try {
							lift.goDown(t);
						}
						catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					// move up or down if no arrival
					else if (!Producer.hasArrival) {
						if (lift.getFloor()==Param.GROUND_FLOOR) {
							try {
								lift.goUp(t);
							}
							catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						else {
							try {
								lift.goDown(t);
							}
							catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				}
				lift.notifyAll();
			}

		}
	}

	public void start()
	{

		if (t == null) {
			t = new Thread(this);
			t.start();
		}
		System.out.println("Operator starts working...");
	}
	
	
	public void interrupt() {
		t.interrupt();
	}

	public boolean isAlive() {
		return t.isAlive();
	}
}
