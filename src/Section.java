/**
 * A section of first floor
 */
public class Section {

    private int number;

	private Car car;

    public Section(int num) {
        number = num;
	    car = null;
    }


    public int getNumber() {
        return number;
    }


	public void loadCar(Car car)
	{
		this.car = car;
		System.out.printf("%s enters section %d\n", car.toString(), number);
	}

    public Car unloadCar()
    {
	    Car temp = this.car;
	    temp = car;
	    car = null;
	    System.out.printf("%s leaves section %d\n", temp.toString(), number);

	    if (temp == null) {
		    System.err.print("temp is null");
		    System.exit(1);
	    }
	    return temp;
    }

    public boolean isOccupied()
    {
	    if (car == null) {
	        return false;
	    }
	    return true;
    }
}
