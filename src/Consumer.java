/**
 * Removes cars that are ready to depart from the lift
 */

public class Consumer implements Runnable {

    private Thread t;

    private Lift lift;

    public Consumer(Lift lift) {
        this.lift = lift;
    }

    @Override
    public void run()
    {
	    while (true)
	    {
			synchronized (lift)
			{
				//System.out.println("consumer waiting...");
				while (!lift.hasOutbound() && lift.getFloor()!=Param.GROUND_FLOOR) {
					try {
						lift.wait();
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				//System.out.println("consumer checking...");
				if (lift.hasOutbound() && lift.getFloor()==Param.GROUND_FLOOR) {

					// unload car from lift and let it go
					Car car = lift.unloadCar();
					// wait for car to depart
					try {
						t.sleep(Param.departureLapse());
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.printf("[%d] departs\n", car.getId());
				}
				lift.notifyAll();
			}
		}
    }

	public void start()
	{
		if (t == null) {
			t = new Thread(this);
			t.start();
		}
		System.out.println("Consumer starts working...");
	}
	
	public void interrupt() {
		t.interrupt();
	}

	public boolean isAlive() {
		return t.isAlive();
	}
}
