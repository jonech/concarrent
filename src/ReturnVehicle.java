/**
 * Tow car from last section to lift
 */
public class ReturnVehicle implements Runnable {

	private Section lastSec;
	private Lift lift;
	private Car towedCar;
	private Thread t;

	public ReturnVehicle(Section lastSec, Lift lift)
	{
		this.lastSec = lastSec;
		this.lift = lift;
	}

	@Override
	public void run()
	{
		while (true) {
			synchronized (lastSec)
			{
				// wait for car to come to last section
				while(!lastSec.isOccupied()) {
					try {
						lastSec.wait();
					}
					catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				if (lastSec.isOccupied()) {
					// tow car from last section
					// cache the car so section can be release
					towedCar = lastSec.unloadCar();
				}
				lastSec.notifyAll();
			}
			// avoid waiting for lift if not waiting car
			while (towedCar!=null) {
				synchronized (lift)
				{
					// wait for lift
					while(lift.getFloor()!=Param.FIRST_FLOOR) {
						try {
							lift.wait();
						}
						catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					if (lift.getFloor()==Param.FIRST_FLOOR &&
							!lift.hasInbound() && !lift.hasOutbound())
					{
						try {
							// take time to tow
							t.sleep(Param.TOWING_TIME);
							// put car back to lift
							lift.loadCar(towedCar);
							towedCar = null;
							lift.goDown(t);
						}
						catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					lift.notifyAll();
				}
			}
		}

	}

	public void start()
	{

		if (t == null) {
			t = new Thread(this);
			t.start();
		}
		System.out.println("Returner starts working...");
	}
	
	public void interrupt() {
		t.interrupt();
	}
}
