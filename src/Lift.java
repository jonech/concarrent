/**
 * This is a lift, a 'bridge' between ground floor and first floor
 */
public class Lift {

    private int floor;
	private Car car;

    public Lift() {
	    floor = Param.GROUND_FLOOR;
    }

	public void goUp(Thread t) throws InterruptedException {
		try {
			if (car == null)
				System.out.println("lift goes up...");
			else
				System.out.printf("%s enters lift to go up\n", car.toString());

			// taking time to go up
			t.sleep(Param.OPERATE_TIME);
			floor += 1;
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void goDown(Thread t) throws InterruptedException {
		try {
			if (car == null)
				System.out.println("lift goes down...");
			else
				System.out.printf("%s enters lift to go down\n", car.toString());

			// taking time to go down
			t.sleep(Param.OPERATE_TIME);
			floor -= 1;
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	public boolean hasOutbound()
	{
		if (car == null) {
			return false;
		}
		else if (car.getBound()== Param.Bound.INBOUND) {
			return false;
		}
		return true;
	}

    public boolean hasInbound()
    {
	    if (car == null) {
		    return false;
	    }
	    else if (car.getBound()== Param.Bound.OUTBOUND) {
		    return false;
	    }
	    return true;
    }

	public int getFloor() {
		return floor;
	}


	public void loadCar(Car car)
	{
		this.car = car;
	}

	public Car unloadCar()
	{
		Car temp = car;
		car = null;
		return temp;
	}

}
