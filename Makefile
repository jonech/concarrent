JFLAGS = -g
JC = javac
.SUFFIXES: .java .class
.java.class:
        $(JC) $(JFLAGS) $*.java

CLASSES = \
        Vehicle.java \
        LaunchVehicle.java \
        ReturnVehicle.java \
        Producer.java \
        Consumer.java \
        Operator.java \
        Section.java \
        Lift.java \
        Car.java \
        Param.java \
        Main.java 

default: classes

classes: $(CLASSES:.java=.class)

clean:
        $(RM) *.class